from locust import HttpLocust, TaskSet, task
import os
import csv
import random
import urllib.parse
import json

from locust.web import app
from src import report
app.add_url_rule('/htmlreport', 'htmlreport', report.download_report)


class UserBehavior( TaskSet):
    success = headers = None
    def on_start(self):
        print (os.path.join ( os.path.dirname(__file__),'recipes.csv' ) )
        """ on_start is called when a Locust start before any task is scheduled """
        self.headers = {
            "X-Native-App": "KochbarApp",
            "X-App-Version": "2.2.4",
            "X-DEVICE-OS": "android",
            "X-Device-Type": "phone"
        }
        self.postLogin()
    @task
    def getRecipe(self):
        self.client.get("/v3/recipe/"+csvReader.getRecipeID(csvReader)[0]+"/similar?limit=4", headers=self.headers)
   
    @task
    def postLogin(self):
        response = self.client.post("/v3/user/login?user="+csvReader.getUser(csvReader)[0]+"&password="+csvReader.getUser(csvReader)[1])
        try:
            self.success = response.json()['success']
            cookie = urllib.parse.quote_plus(response.json()['result']['cookie'])
        except ValueError:
            print(response)
            self.success = False
            cookie = None
        if (self.success):
            cookieHeader = {
                "Cookie":"kochbarde_auth="+cookie
            }
            self.headers.update(cookieHeader)
            self.client.get("/v3/user/me", headers=self.headers)


    @task
    def vote(self):
        recipeID = csvReader.getRecipeID(csvReader)[0]
        self.client.get("/v3/recipe/"+recipeID+"/comment")
        if (self.success):
            response = self.client.get("/v3/recipe/"+recipeID+"/uservoting", headers = self.headers)
            isVoted = response.json()['result']
            if (isVoted):
                data = {
                    "recipeId" : recipeID,
                    "points" : random.randint(1,5)
                }
                self.client.post("/v3/recipe/voting", data = data, headers = self.headers )

    @task
    def fav(self):
        data = {
            "recipeId" : csvReader.getRecipeID(csvReader)[0]
        }
        header = {
            "Content-Type":"application/json; charset = UFT-8"
        }
        self.headers.update(header)
        self.client.post("/v3/recipe/favor", data = json.dumps(data),  headers = self.headers )







class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 0
    max_wait = 0
    

class csvReader:
    userList = list()
    recipeList = list()
    with open(os.path.join ( os.path.dirname(__file__),'users.csv' ) ) as userss:
        userReader = csv.reader(userss, delimiter=';')
        for row in userReader:
            userList.append(row)

    with open( os.path.join ( os.path.dirname(__file__),'recipes.csv' ) ) as recipes:
        recipeReader = csv.reader(recipes, delimiter=';')
        for row in recipeReader:
            recipeList.append(row)

    def getUser(self):
        return self.userList[0]
    def getRecipeID(self):
        return self.recipeList[1]
